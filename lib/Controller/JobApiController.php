<?php

declare(strict_types=1);
// SPDX-FileCopyrightText: Christian Rose <christian.rose@chrose.de>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\Kubernetes\Controller;

use GuzzleHttp\Exception\ClientException;
use OCA\Kubernetes\AppInfo\Application;
use OCA\Kubernetes\Service\JobService;
use OCP\AppFramework\ApiController;
use OCP\AppFramework\Http\DataResponse;
use OCA\Kubernetes\Service\KubernetesService;
use OCP\IRequest;
use GuzzleHttp\Psr7;
use OCP\AppFramework\Http;
use OCP\IConfig;

class JobApiController extends ApiController
{
    private JobService $jobService;
    private KubernetesService $kJobService;
    private IConfig $config;

    public function __construct(
        IRequest $request,
        KubernetesService $kJobService,
        JobService $jobService,
        IConfig $config
    ) {
        parent::__construct(Application::APP_ID, $request);
        $this->kJobService = $kJobService;
        $this->jobService = $jobService;
        $this->config = $config;
    }

    /**
     * @CORS
     * @NoCSRFRequired
     * @NoAdminRequired
     */
    public function index(): DataResponse
    {
        return new DataResponse($this->jobService->findAll());
    }

    /**
     * @CORS
     * @NoCSRFRequired
     * @NoAdminRequired
     */
    public function indexK8s(): DataResponse
    {
        return new DataResponse($this->kJobService->findAll());
    }

    /**
     * @CORS
     * @NoCSRFRequired
     * @NoAdminRequired
     * @PublicPage
     */
    public function push(): DataResponse
    {
        if($this->config->getSystemValue('maintenance', 'false')){
            return new DataResponse('maintenance is active',  Http::STATUS_SERVICE_UNAVAILABLE);
        }
        $token = $this->request->getHeader('X-Auth');
        if ($this->kJobService->isAuthenticate($token)) {
            try {
                return new DataResponse($this->kJobService->push($this->jobService->findAll()));
            } catch (ClientException $e) {
                return new DataResponse([Psr7\Message::toString($e->getResponse()), Psr7\Message::toString($e->getRequest())], $e->getResponse()->getStatusCode());
            }
        }
        return new DataResponse(['not permitted', $token],  Http::STATUS_FORBIDDEN);
    }

    /**
     * @CORS
     * @NoCSRFRequired
     */
    public function schedulePush(): DataResponse
    {
        try {
            return new DataResponse($this->kJobService->schedulePush());
        } catch (ClientException $e) {
            return new DataResponse([Psr7\Message::toString($e->getResponse()), Psr7\Message::toString($e->getRequest())], $e->getResponse()->getStatusCode());
        }
    }
}
