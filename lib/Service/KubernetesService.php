<?php

declare(strict_types=1);
// SPDX-FileCopyrightText: Christian Rose <christian.rose@chrose.de>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\Kubernetes\Service;

use OCP\IConfig;
use \GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use OCA\Kubernetes\AppInfo\Application;
use Psr\Log\LoggerInterface;

class KubernetesService
{
    private HttpClient $client;
    private string $podLabels;
    private string $podTemplate;
    private string $namespace;
    private IConfig $config;
    private LoggerInterface $logger;
    private string $tokenFile;
    private string $containerName;
    private const JOB_PREFIX = 'nc-bg-job-';
    private array $fastJobs = [
        'OCA\Photos\Jobs\MapMediaToPlaceJob' => KubernetesService::JOB_PREFIX . 'map-media-to-place-job'
    ];

    public function __construct(IConfig $config, LoggerInterface $logger)
    {
        $token = $config->getAppValue(Application::APP_ID, 'tokenPath', '/var/run/secrets/kubernetes.io/serviceaccount/token');
        if (file_exists($token)) {
            $this->tokenFile = $token;
            $token = file_get_contents($token);
        }
        if ($token === false) {
            throw new \Exception('Failed to open token file: ' . $this->tokenFile);
        }
        $this->client = new HttpClient([
            'verify' => $config->getAppValue(Application::APP_ID, 'caPath', '/var/run/secrets/kubernetes.io/serviceaccount/ca.crt'),
            'base_uri' => $config->getAppValue(Application::APP_ID, 'cluster', 'https://' . getenv('KUBERNETES_SERVICE_HOST') . ':' . getenv('KUBERNETES_SERVICE_PORT_HTTPS')),
            'headers' => [
                'Authorization' => 'Bearer ' . trim($token),
                'Accept' => 'application/json'
            ]
        ]);
        $this->containerName = $config->getAppValue(Application::APP_ID, 'containerName', 'nextcloud');
        $this->namespace = file_get_contents($config->getAppValue(Application::APP_ID, 'namespacePath', '/var/run/secrets/kubernetes.io/serviceaccount/namespace'));
        $this->podLabels = $config->getAppValue(Application::APP_ID, 'podQuery', 'app.kubernetes.io/component=app,app.kubernetes.io/instance=nextcloud,app.kubernetes.io/name=nextcloud');
        $this->podTemplate = $config->getAppValue(Application::APP_ID, 'podTemplate');
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * 
     */
    private function buildUrl(string $resource, string $version, array $opts = []): string
    {
        $uri = [];
        if (isset($opts['group']) && !is_null($opts['group'])) {
            $uri[] = 'apis/' . $opts['group'];
        } else {
            $uri[] = 'api';
        }
        $uri[] = $version;
        if (isset($opts['namespace']) && $opts['namespace']) {
            $uri[] = 'namespaces/' . $opts['namespace'];
        }
        $uri[] = $resource;
        if (isset($opts['parts']) && is_array($opts['parts'])) {
            $uri = array_merge($uri, $opts['parts']);
        }
        return '/' . implode('/', $uri);
    }

    private function enrichOptsWithDefaults(?array $opts = [], ?array $defaultArgs = []): array
    {
        $defaults = array_merge([
            'group' => null,
            'namespace' => $this->namespace,
            'args' => $defaultArgs
        ], $opts);
        if (isset($opts['args']) && is_array($opts['args'])) {
            $defaults['args'] = array_merge($defaultArgs, $opts['args']);
        }
        return $defaults;
    }

    private function list(string $resource, string $version, array $opts = []): array
    {
        $qualifiedOpts = $this->enrichOptsWithDefaults($opts);
        $response = $this->client->request('GET', $this->buildUrl($resource, $version, $qualifiedOpts), $qualifiedOpts['args']);
        $response = json_decode((string)$response->getBody(), true);
        return $response['items'];
    }

    private function create(string $resource, string $version, mixed $data, array $opts = []): array
    {
        $qualifiedOpts = $this->enrichOptsWithDefaults($opts, ['json' => $data]);
        $response = $this->client->request('POST', $this->buildUrl($resource, $version, $qualifiedOpts), $qualifiedOpts['args']);
        return json_decode((string)$response->getBody(), true);
    }

    private function update(string $id, string $resource, string $version, mixed $data, array $opts = []): array
    {
        $qualifiedOpts = $this->enrichOptsWithDefaults($opts, ['json' => $data]);
        $response = $this->client->request('PUT', $this->buildUrl($resource, $version, $qualifiedOpts) . '/' . $id, $qualifiedOpts['args']);
        return json_decode((string)$response->getBody(), true);
    }

    private function get(string $id, string $resource, string $version, array $opts = []): array
    {
        $qualifiedOpts = $this->enrichOptsWithDefaults($opts);
        $response = $this->client->request('GET', $this->buildUrl($resource, $version, $qualifiedOpts) . '/' . $id, $qualifiedOpts['args']);
        return json_decode((string)$response->getBody(), true);
    }

    /**
     * @return list<Job>
     */
    public function findAll(): array
    {
        return $this->list('jobs', 'v1', ['group' => 'batch']);
    }

    public function getPodTemplate(): array|object
    {
        if ($this->podTemplate) {
            return json_decode($this->podTemplate, true);
        }
        if ($this->podLabels) {
            return $this->list('pods', 'v1', ['args' => ['query' => ['labelSelector' => $this->podLabels]]])[0]['spec'];
        }
        throw new \Exception('No Pod Name or Template given');
    }


    private function jobExists(array $kJobs, string $name): bool
    {
        foreach ($kJobs as $kJob) {
            if ($kJob['metadata']['name'] === $name) {
                return true;
            }
        }
        return false;
    }

    private function addToJobCluster(array $kJobs, array &$jobCluster, string $key, int $id): bool
    {
        if ($this->jobExists($kJobs, $key)) {
            return false;
        }
        if (isset($jobCluster[$key])) {
            $jobCluster[$key]['jobs'][] = $id;
        } else {
            $jobCluster[$key] = [
                'name' => $key,
                'jobs' => array($id)
            ];
        }
        return true;
    }

    public function push(array $jobs): array
    {
        $template = $this->getPodTemplate();
        $this->logger->debug('Searching for Kubernetes jobs');
        $kJobs = $this->findAll();
        $stats = [
            'notDue' => 0,
            'due' => 0,
            'alreadyCreated' => 0,
            'created' => 0,
        ];
        $maxJobs = $this->config->getAppValue(Application::APP_ID, 'maxNewJobs', 10);
        $jobClusters = array();
        foreach ($jobs as $job) {
            if (!$job['isDue']) {
                $stats['notDue']++;
                continue;
            }
            $key = KubernetesService::JOB_PREFIX . $job['id'];
            if (isset($this->fastJobs[$job['class']])) {
                $key = $this->fastJobs[$job['class']];
            }
            if ($this->addToJobCluster($kJobs, $jobClusters, $key, $job['id'])) {
                $stats['due']++;
            } else {
                $stats['alreadyCreated']++;
            }
        }
        $i = count($kJobs);
        foreach ($jobClusters as $cluster) {
            $jobData = [
                'metadata' => [
                    'name' => $cluster['name']
                ],
                'spec' => [
                    'template' => ['spec' => $template],
                    'completions' => 1,
                    'ttlSecondsAfterFinished' => 10
                ]
            ];
            $mainContainer = null;
            foreach ($jobData['spec']['template']['spec']['containers'] as $container) {
                if ($container['name'] === $this->containerName) {
                    $mainContainer = $container;
                    unset($mainContainer['resources']);
                    unset($mainContainer['livenessProbe']);
                    unset($mainContainer['readinessProbe']);
                    unset($mainContainer['lifecycle']);
                    if (count($cluster['jobs']) == 1) {
                        $mainContainer['command'] = ['/var/www/html/occ', 'background-job:execute', '' . $cluster['jobs'][0]];
                    } else {
                        $mainContainer['command'] = ['bash', '-c', 'for j in ' . implode(' ', $cluster['jobs']) . '; do /var/www/html/occ background-job:execute $j; done'];
                    }
                    break;
                }
            }
            $jobData['spec']['template']['spec']['containers'] = array($mainContainer);
            $jobData['spec']['template']['spec']['restartPolicy'] = 'Never';
            $this->logger->debug('Creating Kubernetes job ' . $cluster['name'] . '; Command: ' . print_r($mainContainer['command'], true));
            $this->create('jobs', 'v1', $jobData, ['group' => 'batch']);
            $stats['created']++;
            $i++;
            if ($i >= $maxJobs) {
                $this->logger->debug('Reached max runnings pods (' . $i . '/' . $maxJobs . ')');
                break;
            }
        }
        $this->config->setAppValue('core', 'lastcron', time());
        return $stats;
    }

    public function schedulePush(): array
    {
        $jobData = [
            'metadata' => [
                'name' => 'nextcloud-kubernetes-push'
            ],
            'spec' => [
                'schedule' => "* * * * *",
                'concurrencyPolicy' => 'Forbid',
                'jobTemplate' => [
                    'spec' => [
                        'completions' => 1,
                        'ttlSecondsAfterFinished' => 10,
                        'template' => [
                            'spec' => [
                                'containers' => [
                                    [
                                        'name' => 'nextcloud-kubernetes-push-poll',
                                        'image' =>  'alpine',
                                        'imagePullPolicy' => 'IfNotPresent',
                                        'command' => ['/bin/sh', '-c', 'wget -O - --header "X-Auth: $(cat ' . $this->tokenFile . ')" -T 59 "http://nextcloud.nextcloud.svc.cluster.local:8080/apps/kubernetes/api/v1/Jobs/push"']
                                    ]
                                ],
                                'restartPolicy' => 'OnFailure'
                            ]
                        ]
                    ]
                ],
            ]
        ];
        $exists = false;
        try {
            $exists = $this->get($jobData['metadata']['name'], 'cronjobs', 'v1', ['group' => 'batch']);
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() !== 404) {
                throw $e;
            }
        }
        if ($exists === false) {
            return $this->create('cronjobs', 'v1', $jobData, ['group' => 'batch']);
        } else {
            return $this->update($jobData['metadata']['name'], 'cronjobs', 'v1', $jobData, ['group' => 'batch']);
        }
    }

    public function isAuthenticate(string $token): bool
    {
        $response = $this->create('tokenreviews', 'v1', [
            "metadata" => ["name" => "nc-k8s-validation"],
            "spec" => ["token" => $token]
        ], ['group' => 'authentication.k8s.io', 'namespace' => false]);
        if (isset($response['status']['authenticated']) && is_bool($response['status']['authenticated'])) {
            return $response['status']['authenticated'];
        }
        return false;
    }
}
