<?php

declare(strict_types=1);
// SPDX-FileCopyrightText: Christian Rose <christian.rose@chrose.de>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\Kubernetes\Service;

use OCP\BackgroundJob\IJob;
use OCP\BackgroundJob\IJobList;

class JobService
{
    private IJobList $jobList;

    public function __construct(IJobList $jobList)
    {
        $this->jobList = $jobList;
    }


    private function isDue(IJob $job, int $lastRun, bool $isTimed): bool
    {
        if ($isTimed) {
            $reflection = new \ReflectionClass($job);
            $intervalProperty = $reflection->getProperty('interval');
            $intervalProperty->setAccessible(true);
            $interval = $intervalProperty->getValue($job);

            $nextRun = new \DateTime();
            $nextRun->setTimestamp($lastRun + $interval);

            if ($nextRun <= new \DateTime()) {
                return true;
            }
        } else {
            if ($lastRun < 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return list<Job>
     */
    public function findAll(): array
    {
        $jobs = [];
        foreach ($this->jobList->getJobsIterator(null, null, 0) as $job) {
            $jsonJob = [
                'id' => $job->getId(),
                'class' => get_class($job),
                'argument' => $job->getArgument(),
                'lastRun' => $job->getLastRun(),
                'isTimed' => $job instanceof \OCP\BackgroundJob\TimedJob
            ];
            $jsonJob['isDue'] = $this->isDue($job, $jsonJob['lastRun'], $jsonJob['isTimed']);
            // if ($job instanceof \OCP\BackgroundJob\QueuedJob) {
            //     $jsonJob['interval'] =  $job->interval;
            //     $jsonJob['time'] =  $job->time;
            // }
            $jobs[] = $jsonJob;
        }
        return $jobs;
    }
}
