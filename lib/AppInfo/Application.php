<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: Christian Rose <christian.rose@chrose.de>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\Kubernetes\AppInfo;

use OCP\AppFramework\App;

if (file_exists($autoLoad = __DIR__ . '/../../vendor/autoload.php')) {
	include_once $autoLoad;
}

class Application extends App {
	public const APP_ID = 'kubernetes';

	public function __construct() {
		parent::__construct(self::APP_ID);
	}
}
