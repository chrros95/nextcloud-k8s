if [[ "$1" == "full" ]]; then
  make source
  scp build/artifacts/source/kubernetes.tar.gz 192.168.177.10:
else 
  scp -r {lib,appinfo} 192.168.177.10:/var/data/nextcloud/custom_apps/kubernetes
fi
kubectl logs -l app.kubernetes.io/component=app,app.kubernetes.io/instance=nextcloud,app.kubernetes.io/name=nextcloud -f --tail=10 -n nextcloud -c logger
