// SPDX-FileCopyrightText: Christian Rose <christian.rose@chrose.de>
// SPDX-License-Identifier: AGPL-3.0-or-later
const webpackConfig = require('@nextcloud/webpack-vue-config')

module.exports = webpackConfig
