<?php

declare(strict_types=1);
// SPDX-FileCopyrightText: Christian Rose <christian.rose@chrose.de>
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * Create your routes in here. The name is the lowercase name of the controller
 * without the controller part, the stuff after the hash is the method.
 * e.g. page#index -> OCA\Kubernetes\Controller\PageController->index()
 *
 * The controller class has to be registered in the application.php file since
 * it's instantiated in there
 */
return [
    'resources' => [
        'settings_api' => ['url' => '/api/v1/Settings'],
    ],
    'routes' => [
        ['name' => 'job_api#index', 'url' => '/api/v1/Jobs'],
        ['name' => 'job_api#push', 'url' => '/api/v1/Jobs/push'],
        ['name' => 'job_api#indexK8s', 'url' => '/api/v1/Jobs/k8s'],
        ['name' => 'job_api#schedulePush', 'url' => '/api/v1/Jobs/schedule'],
        ['name' => 'page#index', 'url' => '/', 'verb' => 'GET'],
    ]
];
