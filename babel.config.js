// SPDX-FileCopyrightText: Christian Rose <christian.rose@chrose.de>
// SPDX-License-Identifier: AGPL-3.0-or-later
const babelConfig = require('@nextcloud/babel-config')

module.exports = babelConfig
